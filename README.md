
authentication server

SETUP
- psql -c 'create user postgres createdb'
- psql -c 'create database auth;' -U postgres
- psql -c '\i conf/evolution/schema.sql' -d auth -U postgres

TODO
- verify facebook token against facebook server
- insert facebook id, token into database
- create jwt token for users
- verify jwt tokens

name := "auth-app"

version := "0.1"

organization := "com.rand-gen"

scalaVersion := "2.11.8"

lazy val http4sVersion = "0.14.3a"
lazy val doobieVersion = "0.3.0"
lazy val circeVersion = "0.4.1"

// Only necessary for SNAPSHOT releases
resolvers += Resolver.sonatypeRepo("snapshots")

libraryDependencies ++= Seq(
  "org.scalaz"      %% "scalaz-core"                  % "7.2.4",
  "org.tpolecat"    %% "doobie-core"                  % doobieVersion,
  "org.tpolecat"    %% "doobie-contrib-postgresql"    % doobieVersion,
  "org.tpolecat"    %% "doobie-contrib-specs2"        % doobieVersion,
  "org.http4s"      %% "http4s-dsl"                   % http4sVersion,
  "org.http4s"      %% "http4s-blaze-server"          % http4sVersion,
  "org.http4s"      %% "http4s-blaze-client"          % http4sVersion,
  "org.http4s"      %% "http4s-circe"                 % http4sVersion,
  "org.http4s"      %% "http4s-json4s-jackson"        % http4sVersion,
  "org.slf4j"        % "slf4j-simple"                 % "1.7.21",
  "org.postgresql"   % "postgresql"                   % "9.4-1201-jdbc41",
  "io.circe"        %% "circe-core"                   % circeVersion,
  "io.circe"        %% "circe-generic"                % circeVersion,
  "io.circe"        %% "circe-parser"                 % circeVersion,
  "com.pauldijou"   %% "jwt-circe"                    % "0.8.0"
)

package auth

import scala.util.Random
import doobie.imports._

import scalaz.concurrent.Task
//import io.circe._, io.circe.generic.semiauto._
import io.circe._, io.circe.generic.auto._
import org.http4s._, org.http4s.dsl._
import scalaz._
import Scalaz._

object AccountService {

  implicit def circeJsonDecoder[A](implicit decoder: Decoder[A]) = org.http4s.circe.jsonOf[A]
  implicit def circeJsonEncoder[A](implicit encoder: Encoder[A]) = org.http4s.circe.jsonEncoderOf[A]

  def apply(): HttpService = service

  private val service = HttpService {

    case r @ GET -> Root / "accounts" =>
      Ok(Database.select.transact(Database.xa))

    case r @ POST -> Root / "login" =>
      r.decode[FBToken] { fbtoken =>
        for {
          id       <- FacebookService.getId(fbtoken)
          account  <- Database.safeUpsert(id, fbtoken) transact Database.xa
          response <- account.fold(l => InternalServerError(l.getMessage),
                                   r => Ok(r) addCookie TokenService.generate(id))
        } yield response
      }

    case r @ POST -> Root / "logout" =>
      r.decode[Token] { token =>
        Ok("tests")
      }

    case r @ POST -> Root/ "authenticate" =>
      NotFound("todo: authenticate token")

    // temporary route to mock facebook api
    case r @ GET -> Root / "facebook" =>
      Ok("facebook id: " + 1)
  }
}

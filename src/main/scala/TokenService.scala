/**
  * Created by troilus on 27/08/16.
  */
package auth

import java.time.Instant
import org.http4s.Cookie
import pdi.jwt.{JwtAlgorithm, JwtCirce, JwtClaim}

import scalaz.concurrent.Task

object TokenService {

  val key = "d-cse: dummy project"
  val epr = 3600 * 24 * 365
  val algo = JwtAlgorithm.HS256

  private def claim(id: String) = JwtClaim(
    expiration = Some(Instant.now.plusSeconds(epr).getEpochSecond),
    issuedAt = Some(Instant.now.getEpochSecond),
    subject = Some(id)
  )
  private def token(id: String) = JwtCirce.encode(claim(id), key, algo)

  def generate(id: String): Cookie = Cookie("token", token(id), httpOnly = true)
}

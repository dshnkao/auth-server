package auth

import org.http4s.client.blaze._
import scalaz.concurrent.Task


object FacebookService {

  val client = PooledHttp1Client()

  def getId(token: FBToken): Task[String] = {
    client.expect[String]("http://localhost:8080/api/facebook")
  }
}

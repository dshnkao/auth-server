package auth

import java.sql.SQLException

import scalaz._
import Scalaz._

import doobie.imports._
import doobie.contrib.postgresql.sqlstate.class23.UNIQUE_VIOLATION

import scalaz.concurrent.Task
import scalaz.stream.Process


object Database {

  val xa = DriverManagerTransactor[Task](
    "org.postgresql.Driver", "jdbc:postgresql:auth", "postgres", ""
  )

  def select(id: String) = sql"select id, fbtoken from account where id = $id".query[Account].unique

  def select = sql"select id, fbtoken from account".query[Account].list

  def upsert(id: String, fbtoken: FBToken): ConnectionIO[Account] =
    sql"insert into account (id, fbtoken) values ($id, $fbtoken) on conflict (id) do update set fbtoken = $fbtoken"
      .update
      .withUniqueGeneratedKeys("id", "fbtoken")

  def safeUpsert(id: String, fbtoken: FBToken): ConnectionIO[SQLException \/ Account] = upsert(id, fbtoken).attemptSql
}

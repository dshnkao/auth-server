package auth

case class Account(id: String, fbtoken: String)
case class FBToken(fbtoken: String)
case class Token(token: String)

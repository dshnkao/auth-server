package auth

import org.http4s._, org.http4s.dsl._
import org.http4s.server.{Server, ServerApp}
import org.http4s.server.blaze._
import org.http4s.server.syntax._

import scalaz.concurrent.Task


object AuthApp extends ServerApp {

  val service1 = HttpService {
    case GET -> Root / "hello" / name =>
      Ok(s"Hello, $name.")
  }
  
  val services = service1 orElse AccountService()

  override def server(args: List[String]): Task[Server] = {
    BlazeBuilder
      .bindHttp(8080, "localhost")
      .mountService(services, "/api")
      .start
  }
}
